const axios = require('axios')

const instance = axios.create({
  baseURL: 'htpps://jsonplaceholder.typicode.com'
})

const axiosAdapter = {
  ...instance,
  get: url => instance.get(url)
}

module.exports = axiosAdapter
