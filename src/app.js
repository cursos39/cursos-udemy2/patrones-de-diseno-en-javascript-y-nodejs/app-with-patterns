const express = require('express')
const services = require('./services/index.js')
const { handlers } = require('./handlers')

const app = express()
app.disable('x-powered-by')
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

const USER_RESOURCE = 'users'
const POST_RESOURCE = 'posts'

const userHandler = handlers(USER_RESOURCE)(services)
const productHandler = handlers(POST_RESOURCE)(services)

app.get('/', handlers()(services).get)
app.get(`/${USER_RESOURCE}`, userHandler.getResource)
app.post(`/${USER_RESOURCE}`, userHandler.postResources)
app.put(`/${USER_RESOURCE}/:id`, userHandler.putResourceById)
app.delete(`/${USER_RESOURCE}/:id`, userHandler.deleteResourceById)

app.get(`/${POST_RESOURCE}`, productHandler.getResource)
app.post(`/${POST_RESOURCE}`, productHandler.postResources)
app.put(`/${POST_RESOURCE}/:id`, productHandler.putResourceById)
app.delete(`/${POST_RESOURCE}/:id`, productHandler.deleteResourceById)

module.exports = { app }
