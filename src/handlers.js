
module.exports = {
  handlers: resource => ({ axios }) => {
    return {
      get: (req, res) =>
        res.send('Hello World!'),
      getResource: (req, res) =>
        axios.get(resource)
          .then(({ data }) => res.send(data)),
      postResources: (req, res) =>
        axios.post(resource, req.body)
          .then(({ data }) => res.status(201).send(data)),
      putResourceById: (req, res) =>
        axios.put(`${resource}/${req.params.id}`, req.body)
          .then(({ data }) => res.send(data)),
      deleteResourceById: (req, res) =>
        axios.delete(`${resource}/${req.params.id}`)
          .then(() => res.sendStatus(204))
    }
  }
}
