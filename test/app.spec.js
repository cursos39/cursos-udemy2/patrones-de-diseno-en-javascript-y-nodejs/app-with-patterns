const { expect } = require('chai')
const request = require('supertest')
const { app } = require('../src/app')

describe('App', () => {
  describe('/ services', () => {
    describe('get /', () => {
      it('When get / should return Hello World', () => {
        return request(app).get('/').then(response => expect(response.text).to.be.equal('Hello World!'))
      })
    })
  })
  const resources = ['users', 'posts']
  resources.forEach(resource => {
    describe(`/${resource} services`, () => {
      describe(`get /${resource}`, () => {
        it(`When get ${resource} should return list of ${resource}`, () => {
          return request(app).get(`/${resource}`).then(response => expect(response.body).to.be.an('array'))
        })
      })
      describe(`post /${resource}`, () => {
        it(`When post ${resource} should return 201`, () => {
          return request(app)
            .post(`/${resource}`)
            .send({ name: 'Alberto Eyo' })
            .expect(201)
            .then(response => expect(response.body).to.have.property('id'))
        })
      })
      describe(`put /${resource}/:id`, () => {
        it(`When put ${resource} should return 200`, () => {
          return request(app)
            .put(`/${resource}/1`)
            .send({ name: 'Alberto Eyo' })
            .expect(200)
            .then(response => expect(response.body).to.have.property('id'))
        })
      })
      describe(`delete /${resource}/:id`, () => {
        it(`When delete ${resource} should return 204`, () => {
          return request(app)
            .delete(`/${resource}/1`)
            .expect(204)
        })
      })
    })
  })
})
